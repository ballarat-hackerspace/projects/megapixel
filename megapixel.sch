EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lm3414:LM3414 U4
U 1 1 5BCD8C13
P 1850 3900
F 0 "U4" H 1850 4315 50  0000 C CNN
F 1 "LM3414" H 1850 4224 50  0000 C CNN
F 2 "" H 1850 3900 50  0001 C CNN
F 3 "" H 1850 3900 50  0001 C CNN
	1    1850 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ARGB D1
U 1 1 5BCD8CFE
P 10300 1600
F 0 "D1" V 10346 1270 50  0000 R CNN
F 1 "LED_ARGB" V 10255 1270 50  0000 R CNN
F 2 "" H 10300 1550 50  0001 C CNN
F 3 "~" H 10300 1550 50  0001 C CNN
	1    10300 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5BCD8E07
P 1150 4150
F 0 "R1" H 1209 4196 50  0000 L CNN
F 1 "10k" H 1209 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 1150 4150 50  0001 C CNN
F 3 "~" H 1150 4150 50  0001 C CNN
	1    1150 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5BCD8E3D
P 2350 4250
F 0 "R4" H 2409 4296 50  0000 L CNN
F 1 "47k" H 2409 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 2350 4250 50  0001 C CNN
F 3 "~" H 2350 4250 50  0001 C CNN
	1    2350 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5BCD8EE3
P 1150 3750
F 0 "C8" V 921 3750 50  0000 C CNN
F 1 "1uF 16V" V 1012 3750 50  0000 C CNN
F 2 "" H 1150 3750 50  0001 C CNN
F 3 "~" H 1150 3750 50  0001 C CNN
	1    1150 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5BCD8F2B
P 3500 3550
F 0 "C5" H 3592 3596 50  0000 L CNN
F 1 "2u2 100V" H 3592 3505 50  0000 L CNN
F 2 "" H 3500 3550 50  0001 C CNN
F 3 "~" H 3500 3550 50  0001 C CNN
	1    3500 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 5BCD8FD0
P 2450 3600
F 0 "D3" V 2404 3679 50  0000 L CNN
F 1 "100V 2A" V 2500 3700 50  0000 L CNN
F 2 "" H 2450 3600 50  0001 C CNN
F 3 "~" H 2450 3600 50  0001 C CNN
	1    2450 3600
	0    1    1    0   
$EndComp
$Comp
L pspice:INDUCTOR L2
U 1 1 5BCD90CA
P 2800 3850
F 0 "L2" H 2800 3950 50  0000 C CNN
F 1 "47uH" H 2800 3800 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D9.5mm_P5.00mm_Fastron_07HVP" H 2800 3850 50  0001 C CNN
F 3 "" H 2800 3850 50  0001 C CNN
	1    2800 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR023
U 1 1 5BCD9187
P 850 3950
F 0 "#PWR023" H 850 3700 50  0001 C CNN
F 1 "GNDREF" H 855 3777 50  0000 C CNN
F 2 "" H 850 3950 50  0001 C CNN
F 3 "" H 850 3950 50  0001 C CNN
	1    850  3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3750 1250 3750
Wire Wire Line
	1050 3750 850  3750
Wire Wire Line
	850  3750 850  3850
Wire Wire Line
	850  3850 1450 3850
Wire Wire Line
	850  3950 850  3850
Connection ~ 850  3850
Wire Wire Line
	1150 4050 1150 3950
Wire Wire Line
	1150 3950 1450 3950
Wire Wire Line
	1450 4050 1350 4050
Wire Wire Line
	1350 4050 1350 4350
Wire Wire Line
	1350 4350 1150 4350
Wire Wire Line
	1150 4350 1150 4250
Wire Wire Line
	1150 4450 1150 4350
Connection ~ 1150 4350
$Comp
L power:GNDREF #PWR029
U 1 1 5BCD9678
P 1150 4450
F 0 "#PWR029" H 1150 4200 50  0001 C CNN
F 1 "GNDREF" H 1155 4277 50  0000 C CNN
F 2 "" H 1150 4450 50  0001 C CNN
F 3 "" H 1150 4450 50  0001 C CNN
	1    1150 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR026
U 1 1 5BCD9693
P 1850 4250
F 0 "#PWR026" H 1850 4000 50  0001 C CNN
F 1 "GNDREF" H 1855 4077 50  0000 C CNN
F 2 "" H 1850 4250 50  0001 C CNN
F 3 "" H 1850 4250 50  0001 C CNN
	1    1850 4250
	1    0    0    -1  
$EndComp
Text Label 2250 3950 0    50   ~ 0
RED_PWM
Wire Wire Line
	2250 3850 2450 3850
Wire Wire Line
	2450 3750 2450 3850
Connection ~ 2450 3850
Wire Wire Line
	2450 3850 2550 3850
Wire Wire Line
	2250 3750 2350 3750
Wire Wire Line
	2350 3750 2350 3350
Wire Wire Line
	2350 3350 2450 3350
Wire Wire Line
	2450 3350 2450 3450
Text Label 3050 3850 0    50   ~ 0
RED_K
Wire Wire Line
	2450 3350 3500 3350
Wire Wire Line
	3500 3350 3500 3450
Connection ~ 2450 3350
$Comp
L power:GNDREF #PWR020
U 1 1 5BCD9D21
P 3500 3750
F 0 "#PWR020" H 3500 3500 50  0001 C CNN
F 1 "GNDREF" H 3505 3577 50  0000 C CNN
F 2 "" H 3500 3750 50  0001 C CNN
F 3 "" H 3500 3750 50  0001 C CNN
	1    3500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3650 3500 3750
Wire Wire Line
	2250 4050 2350 4050
Wire Wire Line
	2350 4050 2350 4150
Wire Wire Line
	2350 4450 2350 4350
$Comp
L power:GNDREF #PWR030
U 1 1 5BCDA2AF
P 2350 4450
F 0 "#PWR030" H 2350 4200 50  0001 C CNN
F 1 "GNDREF" H 2355 4277 50  0000 C CNN
F 2 "" H 2350 4450 50  0001 C CNN
F 3 "" H 2350 4450 50  0001 C CNN
	1    2350 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR017
U 1 1 5BCDA2FE
P 2450 3350
F 0 "#PWR017" H 2450 3200 50  0001 C CNN
F 1 "VCC" H 2467 3523 50  0000 C CNN
F 2 "" H 2450 3350 50  0001 C CNN
F 3 "" H 2450 3350 50  0001 C CNN
	1    2450 3350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR08
U 1 1 5BCDAC6D
P 10300 1400
F 0 "#PWR08" H 10300 1250 50  0001 C CNN
F 1 "VCC" H 10317 1573 50  0000 C CNN
F 2 "" H 10300 1400 50  0001 C CNN
F 3 "" H 10300 1400 50  0001 C CNN
	1    10300 1400
	1    0    0    -1  
$EndComp
Text Label 10100 1800 3    50   ~ 0
RED_K
Text Label 10300 1800 3    50   ~ 0
GREEN_K
Text Label 10500 1800 3    50   ~ 0
BLUE_K
$Comp
L Driver_LED:WS2811 U3
U 1 1 5BCDAD72
P 7250 1650
F 0 "U3" H 7000 1900 50  0000 C CNN
F 1 "WS2811" H 7450 1900 50  0000 C CNN
F 2 "Package_SO:SO-8_5.3x6.2mm_P1.27mm" H 6950 1800 50  0001 C CNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2811.pdf" H 7050 1900 50  0001 C CNN
	1    7250 1650
	1    0    0    -1  
$EndComp
Text Label 7650 1550 0    50   ~ 0
RED_PWM
Text Label 7650 1650 0    50   ~ 0
GREEN_PWM
Text Label 7650 1750 0    50   ~ 0
BLUE_PWM
Text Label 6850 1550 2    50   ~ 0
DIN
Text Label 6850 1650 2    50   ~ 0
DOUT
$Comp
L power:GNDREF #PWR09
U 1 1 5BCDB04F
P 7250 1950
F 0 "#PWR09" H 7250 1700 50  0001 C CNN
F 1 "GNDREF" H 7255 1777 50  0000 C CNN
F 2 "" H 7250 1950 50  0001 C CNN
F 3 "" H 7250 1950 50  0001 C CNN
	1    7250 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5BCDB0F8
P 7250 1350
F 0 "#PWR01" H 7250 1200 50  0001 C CNN
F 1 "+5V" H 7265 1523 50  0000 C CNN
F 2 "" H 7250 1350 50  0001 C CNN
F 3 "" H 7250 1350 50  0001 C CNN
	1    7250 1350
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM2937xT U1
U 1 1 5BCDB1F9
P 5750 1500
F 0 "U1" H 5750 1742 50  0000 C CNN
F 1 "LM2937xT" H 5750 1651 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5750 1725 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2937.pdf" H 5750 1450 50  0001 C CNN
	1    5750 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5BCDB255
P 5350 1400
F 0 "#PWR06" H 5350 1250 50  0001 C CNN
F 1 "+5V" H 5365 1573 50  0000 C CNN
F 2 "" H 5350 1400 50  0001 C CNN
F 3 "" H 5350 1400 50  0001 C CNN
	1    5350 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR012
U 1 1 5BCDB274
P 5750 2000
F 0 "#PWR012" H 5750 1750 50  0001 C CNN
F 1 "GNDREF" H 5755 1827 50  0000 C CNN
F 2 "" H 5750 2000 50  0001 C CNN
F 3 "" H 5750 2000 50  0001 C CNN
	1    5750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 5BCDB2BB
P 6150 1400
F 0 "#PWR07" H 6150 1250 50  0001 C CNN
F 1 "+3V3" H 6165 1573 50  0000 C CNN
F 2 "" H 6150 1400 50  0001 C CNN
F 3 "" H 6150 1400 50  0001 C CNN
	1    6150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1400 5350 1500
Wire Wire Line
	5350 1500 5450 1500
Wire Wire Line
	6050 1500 6150 1500
Wire Wire Line
	6150 1500 6150 1400
Text Notes 9750 4900 0    39   ~ 0
Input Voltage = 36V\nLED String Voltage (R) = 24V\nLED String Voltage (G,B) = 34V\nLED Current = 310mA\nSwitching Frequency = 425kHz\nMaximum LED Current Ripple <= 500mA\nMaximum Input Voltage Ripple <= 200mV
Text Notes 1700 3000 0    50   ~ 10
Current Drive (RED)
$Comp
L lm3414:LM3414 U5
U 1 1 5BCDB126
P 5300 3900
F 0 "U5" H 5300 4315 50  0000 C CNN
F 1 "LM3414" H 5300 4224 50  0000 C CNN
F 2 "" H 5300 3900 50  0001 C CNN
F 3 "" H 5300 3900 50  0001 C CNN
	1    5300 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5BCDB12C
P 4600 4150
F 0 "R2" H 4659 4196 50  0000 L CNN
F 1 "10k" H 4659 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 4600 4150 50  0001 C CNN
F 3 "~" H 4600 4150 50  0001 C CNN
	1    4600 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5BCDB132
P 5800 4250
F 0 "R5" H 5859 4296 50  0000 L CNN
F 1 "47k" H 5859 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 5800 4250 50  0001 C CNN
F 3 "~" H 5800 4250 50  0001 C CNN
	1    5800 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5BCDB138
P 4600 3750
F 0 "C9" V 4371 3750 50  0000 C CNN
F 1 "1uF 16V" V 4462 3750 50  0000 C CNN
F 2 "" H 4600 3750 50  0001 C CNN
F 3 "~" H 4600 3750 50  0001 C CNN
	1    4600 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5BCDB13E
P 6950 3550
F 0 "C6" H 7042 3596 50  0000 L CNN
F 1 "2u2 100V" H 7042 3505 50  0000 L CNN
F 2 "" H 6950 3550 50  0001 C CNN
F 3 "~" H 6950 3550 50  0001 C CNN
	1    6950 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:D D4
U 1 1 5BCDB144
P 5900 3600
F 0 "D4" V 5854 3679 50  0000 L CNN
F 1 "100V 2A" V 5950 3700 50  0000 L CNN
F 2 "" H 5900 3600 50  0001 C CNN
F 3 "~" H 5900 3600 50  0001 C CNN
	1    5900 3600
	0    1    1    0   
$EndComp
$Comp
L pspice:INDUCTOR L3
U 1 1 5BCDB14A
P 6250 3850
F 0 "L3" H 6250 3950 50  0000 C CNN
F 1 "47uH" H 6250 3800 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D9.5mm_P5.00mm_Fastron_07HVP" H 6250 3850 50  0001 C CNN
F 3 "" H 6250 3850 50  0001 C CNN
	1    6250 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR024
U 1 1 5BCDB150
P 4300 3950
F 0 "#PWR024" H 4300 3700 50  0001 C CNN
F 1 "GNDREF" H 4305 3777 50  0000 C CNN
F 2 "" H 4300 3950 50  0001 C CNN
F 3 "" H 4300 3950 50  0001 C CNN
	1    4300 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3750 4700 3750
Wire Wire Line
	4500 3750 4300 3750
Wire Wire Line
	4300 3750 4300 3850
Wire Wire Line
	4300 3850 4900 3850
Wire Wire Line
	4300 3950 4300 3850
Connection ~ 4300 3850
Wire Wire Line
	4600 4050 4600 3950
Wire Wire Line
	4600 3950 4900 3950
Wire Wire Line
	4900 4050 4800 4050
Wire Wire Line
	4800 4050 4800 4350
Wire Wire Line
	4800 4350 4600 4350
Wire Wire Line
	4600 4350 4600 4250
Wire Wire Line
	4600 4450 4600 4350
Connection ~ 4600 4350
$Comp
L power:GNDREF #PWR031
U 1 1 5BCDB164
P 4600 4450
F 0 "#PWR031" H 4600 4200 50  0001 C CNN
F 1 "GNDREF" H 4605 4277 50  0000 C CNN
F 2 "" H 4600 4450 50  0001 C CNN
F 3 "" H 4600 4450 50  0001 C CNN
	1    4600 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR027
U 1 1 5BCDB16A
P 5300 4250
F 0 "#PWR027" H 5300 4000 50  0001 C CNN
F 1 "GNDREF" H 5305 4077 50  0000 C CNN
F 2 "" H 5300 4250 50  0001 C CNN
F 3 "" H 5300 4250 50  0001 C CNN
	1    5300 4250
	1    0    0    -1  
$EndComp
Text Label 5700 3950 0    50   ~ 0
GREEN_PWM
Wire Wire Line
	5700 3850 5900 3850
Wire Wire Line
	5900 3750 5900 3850
Connection ~ 5900 3850
Wire Wire Line
	5900 3850 6000 3850
Wire Wire Line
	5700 3750 5800 3750
Wire Wire Line
	5800 3750 5800 3350
Wire Wire Line
	5800 3350 5900 3350
Wire Wire Line
	5900 3350 5900 3450
Text Label 6500 3850 0    50   ~ 0
GREEN_K
Wire Wire Line
	5900 3350 6950 3350
Wire Wire Line
	6950 3350 6950 3450
Connection ~ 5900 3350
$Comp
L power:GNDREF #PWR021
U 1 1 5BCDB17D
P 6950 3750
F 0 "#PWR021" H 6950 3500 50  0001 C CNN
F 1 "GNDREF" H 6955 3577 50  0000 C CNN
F 2 "" H 6950 3750 50  0001 C CNN
F 3 "" H 6950 3750 50  0001 C CNN
	1    6950 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3650 6950 3750
Wire Wire Line
	5700 4050 5800 4050
Wire Wire Line
	5800 4050 5800 4150
Wire Wire Line
	5800 4450 5800 4350
$Comp
L power:GNDREF #PWR032
U 1 1 5BCDB187
P 5800 4450
F 0 "#PWR032" H 5800 4200 50  0001 C CNN
F 1 "GNDREF" H 5805 4277 50  0000 C CNN
F 2 "" H 5800 4450 50  0001 C CNN
F 3 "" H 5800 4450 50  0001 C CNN
	1    5800 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR018
U 1 1 5BCDB18D
P 5900 3350
F 0 "#PWR018" H 5900 3200 50  0001 C CNN
F 1 "VCC" H 5917 3523 50  0000 C CNN
F 2 "" H 5900 3350 50  0001 C CNN
F 3 "" H 5900 3350 50  0001 C CNN
	1    5900 3350
	1    0    0    -1  
$EndComp
Text Notes 5150 3000 0    50   ~ 10
Current Drive (GREEN)
$Comp
L lm3414:LM3414 U6
U 1 1 5BCDC946
P 8700 3900
F 0 "U6" H 8700 4315 50  0000 C CNN
F 1 "LM3414" H 8700 4224 50  0000 C CNN
F 2 "" H 8700 3900 50  0001 C CNN
F 3 "" H 8700 3900 50  0001 C CNN
	1    8700 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5BCDC94C
P 8000 4150
F 0 "R3" H 8059 4196 50  0000 L CNN
F 1 "10k" H 8059 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 8000 4150 50  0001 C CNN
F 3 "~" H 8000 4150 50  0001 C CNN
	1    8000 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5BCDC952
P 9200 4250
F 0 "R6" H 9259 4296 50  0000 L CNN
F 1 "47k" H 9259 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 9200 4250 50  0001 C CNN
F 3 "~" H 9200 4250 50  0001 C CNN
	1    9200 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5BCDC958
P 8000 3750
F 0 "C10" V 7771 3750 50  0000 C CNN
F 1 "1uF 16V" V 7862 3750 50  0000 C CNN
F 2 "" H 8000 3750 50  0001 C CNN
F 3 "~" H 8000 3750 50  0001 C CNN
	1    8000 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5BCDC95E
P 10350 3550
F 0 "C7" H 10442 3596 50  0000 L CNN
F 1 "2u2 100V" H 10442 3505 50  0000 L CNN
F 2 "" H 10350 3550 50  0001 C CNN
F 3 "~" H 10350 3550 50  0001 C CNN
	1    10350 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:D D5
U 1 1 5BCDC964
P 9300 3600
F 0 "D5" V 9254 3679 50  0000 L CNN
F 1 "100V 2A" V 9350 3700 50  0000 L CNN
F 2 "" H 9300 3600 50  0001 C CNN
F 3 "~" H 9300 3600 50  0001 C CNN
	1    9300 3600
	0    1    1    0   
$EndComp
$Comp
L pspice:INDUCTOR L4
U 1 1 5BCDC96A
P 9650 3850
F 0 "L4" H 9650 3950 50  0000 C CNN
F 1 "47uH" H 9650 3800 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D9.5mm_P5.00mm_Fastron_07HVP" H 9650 3850 50  0001 C CNN
F 3 "" H 9650 3850 50  0001 C CNN
	1    9650 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR025
U 1 1 5BCDC970
P 7700 3950
F 0 "#PWR025" H 7700 3700 50  0001 C CNN
F 1 "GNDREF" H 7705 3777 50  0000 C CNN
F 2 "" H 7700 3950 50  0001 C CNN
F 3 "" H 7700 3950 50  0001 C CNN
	1    7700 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 3750 8100 3750
Wire Wire Line
	7900 3750 7700 3750
Wire Wire Line
	7700 3750 7700 3850
Wire Wire Line
	7700 3850 8300 3850
Wire Wire Line
	7700 3950 7700 3850
Connection ~ 7700 3850
Wire Wire Line
	8000 4050 8000 3950
Wire Wire Line
	8000 3950 8300 3950
Wire Wire Line
	8300 4050 8200 4050
Wire Wire Line
	8200 4050 8200 4350
Wire Wire Line
	8200 4350 8000 4350
Wire Wire Line
	8000 4350 8000 4250
Wire Wire Line
	8000 4450 8000 4350
Connection ~ 8000 4350
$Comp
L power:GNDREF #PWR033
U 1 1 5BCDC984
P 8000 4450
F 0 "#PWR033" H 8000 4200 50  0001 C CNN
F 1 "GNDREF" H 8005 4277 50  0000 C CNN
F 2 "" H 8000 4450 50  0001 C CNN
F 3 "" H 8000 4450 50  0001 C CNN
	1    8000 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR028
U 1 1 5BCDC98A
P 8700 4250
F 0 "#PWR028" H 8700 4000 50  0001 C CNN
F 1 "GNDREF" H 8705 4077 50  0000 C CNN
F 2 "" H 8700 4250 50  0001 C CNN
F 3 "" H 8700 4250 50  0001 C CNN
	1    8700 4250
	1    0    0    -1  
$EndComp
Text Label 9100 3950 0    50   ~ 0
BLUE_PWM
Wire Wire Line
	9100 3850 9300 3850
Wire Wire Line
	9300 3750 9300 3850
Connection ~ 9300 3850
Wire Wire Line
	9300 3850 9400 3850
Wire Wire Line
	9100 3750 9200 3750
Wire Wire Line
	9200 3750 9200 3350
Wire Wire Line
	9200 3350 9300 3350
Wire Wire Line
	9300 3350 9300 3450
Text Label 9900 3850 0    50   ~ 0
BLUE_K
Wire Wire Line
	9300 3350 10350 3350
Wire Wire Line
	10350 3350 10350 3450
Connection ~ 9300 3350
$Comp
L power:GNDREF #PWR022
U 1 1 5BCDC99D
P 10350 3750
F 0 "#PWR022" H 10350 3500 50  0001 C CNN
F 1 "GNDREF" H 10355 3577 50  0000 C CNN
F 2 "" H 10350 3750 50  0001 C CNN
F 3 "" H 10350 3750 50  0001 C CNN
	1    10350 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3650 10350 3750
Wire Wire Line
	9100 4050 9200 4050
Wire Wire Line
	9200 4050 9200 4150
Wire Wire Line
	9200 4450 9200 4350
$Comp
L power:GNDREF #PWR034
U 1 1 5BCDC9A7
P 9200 4450
F 0 "#PWR034" H 9200 4200 50  0001 C CNN
F 1 "GNDREF" H 9205 4277 50  0000 C CNN
F 2 "" H 9200 4450 50  0001 C CNN
F 3 "" H 9200 4450 50  0001 C CNN
	1    9200 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR019
U 1 1 5BCDC9AD
P 9300 3350
F 0 "#PWR019" H 9300 3200 50  0001 C CNN
F 1 "VCC" H 9317 3523 50  0000 C CNN
F 2 "" H 9300 3350 50  0001 C CNN
F 3 "" H 9300 3350 50  0001 C CNN
	1    9300 3350
	1    0    0    -1  
$EndComp
Text Notes 8550 3000 0    50   ~ 10
Current Drive (BLUE)
Text Notes 10150 1050 0    50   ~ 10
LED (RGB)
NoConn ~ 6850 1750
$Comp
L Connector:Barrel_Jack_MountingPin J1
U 1 1 5BCEAC48
P 1150 1600
F 0 "J1" H 1205 1826 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 1205 1826 50  0001 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 1200 1560 50  0001 C CNN
F 3 "~" H 1200 1560 50  0001 C CNN
	1    1150 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR013
U 1 1 5BCEADE3
P 1150 2100
F 0 "#PWR013" H 1150 1850 50  0001 C CNN
F 1 "GNDREF" H 1155 1927 50  0000 C CNN
F 2 "" H 1150 2100 50  0001 C CNN
F 3 "" H 1150 2100 50  0001 C CNN
	1    1150 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1900 1150 2000
Wire Wire Line
	1150 2000 1550 2000
Wire Wire Line
	1550 2000 1550 1700
Wire Wire Line
	1550 1700 1450 1700
Connection ~ 1150 2000
Wire Wire Line
	1150 2000 1150 2100
$Comp
L power:VCC #PWR04
U 1 1 5BCEED23
P 1550 1400
F 0 "#PWR04" H 1550 1250 50  0001 C CNN
F 1 "VCC" H 1567 1573 50  0000 C CNN
F 2 "" H 1550 1400 50  0001 C CNN
F 3 "" H 1550 1400 50  0001 C CNN
	1    1550 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1400 1550 1500
Wire Wire Line
	1550 1500 1450 1500
Text Notes 600  1300 0    50   ~ 0
Vin = 36V - 48V
Text Notes 1050 1050 0    50   ~ 10
Power Input
$Comp
L Device:C_Small C3
U 1 1 5BCF2499
P 6150 1700
F 0 "C3" H 6242 1746 50  0000 L CNN
F 1 "100n" H 6242 1655 50  0000 L CNN
F 2 "" H 6150 1700 50  0001 C CNN
F 3 "~" H 6150 1700 50  0001 C CNN
	1    6150 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 1800 5750 1900
Wire Wire Line
	5750 1900 6150 1900
Wire Wire Line
	6150 1900 6150 1800
Wire Wire Line
	6150 1600 6150 1500
Connection ~ 6150 1500
Connection ~ 5750 1900
Wire Wire Line
	5750 1900 5750 2000
$Comp
L Regulator_Switching:LM2596T-5 U2
U 1 1 5BCF9EFF
P 3250 1600
F 0 "U2" H 3250 1967 50  0000 C CNN
F 1 "LM2596HV-5.0" H 3250 1876 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-5_P3.4x3.7mm_StaggerOdd_Lead3.8mm_Vertical" H 3300 1350 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2596.pdf" H 3250 1600 50  0001 C CNN
	1    3250 1600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5BCFBDD7
P 2250 1400
F 0 "#PWR05" H 2250 1250 50  0001 C CNN
F 1 "VCC" H 2267 1573 50  0000 C CNN
F 2 "" H 2250 1400 50  0001 C CNN
F 3 "" H 2250 1400 50  0001 C CNN
	1    2250 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1700 2650 1700
Wire Wire Line
	2650 1700 2650 2000
Wire Wire Line
	2650 2000 3250 2000
Wire Wire Line
	3250 2000 3250 1900
Wire Wire Line
	3250 2100 3250 2000
Connection ~ 3250 2000
$Comp
L power:GNDREF #PWR014
U 1 1 5BD00123
P 3250 2100
F 0 "#PWR014" H 3250 1850 50  0001 C CNN
F 1 "GNDREF" H 3255 1927 50  0000 C CNN
F 2 "" H 3250 2100 50  0001 C CNN
F 3 "" H 3250 2100 50  0001 C CNN
	1    3250 2100
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 5BD0236E
P 4200 1700
F 0 "L1" H 4200 1800 50  0000 C CNN
F 1 "68uH" H 4200 1650 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D9.5mm_P5.00mm_Fastron_07HVP" H 4200 1700 50  0001 C CNN
F 3 "" H 4200 1700 50  0001 C CNN
	1    4200 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D2
U 1 1 5BD024CA
P 3850 1900
F 0 "D2" V 3804 1979 50  0000 L CNN
F 1 "1N5822" V 3900 2000 50  0000 L CNN
F 2 "" H 3850 1900 50  0001 C CNN
F 3 "~" H 3850 1900 50  0001 C CNN
	1    3850 1900
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR015
U 1 1 5BD0260D
P 3850 2100
F 0 "#PWR015" H 3850 1850 50  0001 C CNN
F 1 "GNDREF" H 3855 1927 50  0000 C CNN
F 2 "" H 3850 2100 50  0001 C CNN
F 3 "" H 3850 2100 50  0001 C CNN
	1    3850 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1500 4550 1500
Wire Wire Line
	4550 1500 4550 1700
Wire Wire Line
	4550 1700 4450 1700
$Comp
L Device:C_Small C2
U 1 1 5BD0B8A9
P 2450 1700
F 0 "C2" H 2400 1800 50  0000 C CNN
F 1 "100n 63V" V 2400 1450 50  0000 C CNN
F 2 "" H 2450 1700 50  0001 C CNN
F 3 "~" H 2450 1700 50  0001 C CNN
	1    2450 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 5BD0BA3D
P 4550 1900
F 0 "C4" H 4638 1946 50  0000 L CNN
F 1 "470u" H 4638 1855 50  0000 L CNN
F 2 "" H 4550 1900 50  0001 C CNN
F 3 "~" H 4550 1900 50  0001 C CNN
	1    4550 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C1
U 1 1 5BD0BAED
P 2250 1700
F 0 "C1" H 2100 1800 50  0000 L CNN
F 1 "470uF 63V" V 2150 1250 50  0000 L CNN
F 2 "" H 2250 1700 50  0001 C CNN
F 3 "~" H 2250 1700 50  0001 C CNN
	1    2250 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1800 2250 2000
Wire Wire Line
	2250 2000 2450 2000
Wire Wire Line
	2450 1800 2450 2000
Connection ~ 2450 2000
Wire Wire Line
	2450 2000 2650 2000
Wire Wire Line
	2250 1600 2250 1500
Wire Wire Line
	2250 1500 2450 1500
Wire Wire Line
	2450 1600 2450 1500
Connection ~ 2450 1500
Wire Wire Line
	2450 1500 2750 1500
Wire Wire Line
	2250 1400 2250 1500
Connection ~ 2250 1500
Connection ~ 2650 2000
$Comp
L power:GNDREF #PWR016
U 1 1 5BD23EEC
P 4550 2100
F 0 "#PWR016" H 4550 1850 50  0001 C CNN
F 1 "GNDREF" H 4555 1927 50  0000 C CNN
F 2 "" H 4550 2100 50  0001 C CNN
F 3 "" H 4550 2100 50  0001 C CNN
	1    4550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2100 4550 2000
Wire Wire Line
	4550 1800 4550 1700
Connection ~ 4550 1700
Wire Wire Line
	3750 1700 3850 1700
Wire Wire Line
	3850 2100 3850 2050
Wire Wire Line
	3850 1750 3850 1700
Connection ~ 3850 1700
Wire Wire Line
	3850 1700 3950 1700
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5BD30E44
P 4850 1300
F 0 "J2" V 4770 1112 50  0000 R CNN
F 1 "Conn_01x02" V 4725 1112 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4850 1300 50  0001 C CNN
F 3 "~" H 4850 1300 50  0001 C CNN
	1    4850 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 1500 4850 1500
Connection ~ 4550 1500
Wire Wire Line
	4950 1500 5350 1500
Connection ~ 5350 1500
Text Notes 4350 1050 0    50   ~ 10
Power Conversion
Text Notes 7500 1050 0    50   ~ 10
LED Control
$Comp
L power:+3V3 #PWR035
U 1 1 5BD3C309
P 1100 5550
F 0 "#PWR035" H 1100 5400 50  0001 C CNN
F 1 "+3V3" H 1115 5723 50  0000 C CNN
F 2 "" H 1100 5550 50  0001 C CNN
F 3 "" H 1100 5550 50  0001 C CNN
	1    1100 5550
	1    0    0    -1  
$EndComp
$Comp
L ESP8266:ESP-01 U7
U 1 1 5BD4040F
P 2150 6000
F 0 "U7" H 2150 6515 50  0000 C CNN
F 1 "ESP-01" H 2150 6424 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 2150 6000 50  0001 C CNN
F 3 "http://l0l.org.uk/2014/12/esp8266-modules-hardware-guide-gotta-catch-em-all/" H 2150 6000 50  0001 C CNN
	1    2150 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR036
U 1 1 5BD40535
P 2900 6350
F 0 "#PWR036" H 2900 6100 50  0001 C CNN
F 1 "GNDREF" H 2905 6177 50  0000 C CNN
F 2 "" H 2900 6350 50  0001 C CNN
F 3 "" H 2900 6350 50  0001 C CNN
	1    2900 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5850 2900 5850
Wire Wire Line
	2900 5850 2900 6350
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5BD44373
P 3000 5550
F 0 "J5" V 2966 5362 50  0000 R CNN
F 1 "AUTO" V 3150 5600 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3000 5550 50  0001 C CNN
F 3 "~" H 3000 5550 50  0001 C CNN
	1    3000 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2800 5950 3000 5950
Wire Wire Line
	3000 5950 3000 5750
Text Label 3100 5750 0    50   ~ 0
DIN
$Comp
L Connector_Generic:Conn_01x06 J6
U 1 1 5BD484FF
P 3750 5950
F 0 "J6" H 3829 5942 50  0000 L CNN
F 1 "SERIAL" H 3829 5851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 3750 5950 50  0001 C CNN
F 3 "~" H 3750 5950 50  0001 C CNN
	1    3750 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR037
U 1 1 5BD4C0FB
P 3450 6350
F 0 "#PWR037" H 3450 6100 50  0001 C CNN
F 1 "GNDREF" H 3455 6177 50  0000 C CNN
F 2 "" H 3450 6350 50  0001 C CNN
F 3 "" H 3450 6350 50  0001 C CNN
	1    3450 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6350 3450 6250
Wire Wire Line
	3450 6250 3550 6250
Text Label 2800 6150 0    50   ~ 0
RX
Text Label 3550 5950 2    50   ~ 0
RX
Text Label 1500 5850 2    50   ~ 0
TX
Text Label 3550 5850 2    50   ~ 0
TX
$Comp
L Switch:SW_Push SW1
U 1 1 5BD5001F
P 1100 6350
F 0 "SW1" V 1146 6302 50  0000 R CNN
F 1 "SW_Push" V 1055 6302 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_EVQPE1" H 1100 6550 50  0001 C CNN
F 3 "" H 1100 6550 50  0001 C CNN
	1    1100 6350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5BD5B77B
P 1100 5850
F 0 "R7" H 950 5900 50  0000 L CNN
F 1 "10k" H 900 5800 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 1100 5850 50  0001 C CNN
F 3 "~" H 1100 5850 50  0001 C CNN
	1    1100 5850
	1    0    0    -1  
$EndComp
NoConn ~ 2800 6050
Wire Wire Line
	1100 5550 1100 5650
Wire Wire Line
	1100 5650 1300 5650
Wire Wire Line
	1300 5650 1300 5950
Wire Wire Line
	1300 5950 1500 5950
Connection ~ 1100 5650
Wire Wire Line
	1100 5650 1100 5750
Wire Wire Line
	1300 5950 1300 6150
Wire Wire Line
	1300 6150 1500 6150
Connection ~ 1300 5950
Wire Wire Line
	1100 6050 1100 5950
Wire Wire Line
	1100 6050 1100 6150
Connection ~ 1100 6050
$Comp
L power:GNDREF #PWR038
U 1 1 5BD74D22
P 1100 6650
F 0 "#PWR038" H 1100 6400 50  0001 C CNN
F 1 "GNDREF" H 1105 6477 50  0000 C CNN
F 2 "" H 1100 6650 50  0001 C CNN
F 3 "" H 1100 6650 50  0001 C CNN
	1    1100 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 6650 1100 6550
Wire Wire Line
	1500 6050 1100 6050
Text Notes 2000 5200 0    50   ~ 10
WiFi Control
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5BDE2DA9
P 8600 1650
F 0 "J3" H 8600 1850 50  0000 L CNN
F 1 "CHAIN" H 8680 1601 50  0001 L CNN
F 2 "" H 8600 1650 50  0001 C CNN
F 3 "~" H 8600 1650 50  0001 C CNN
	1    8600 1650
	1    0    0    -1  
$EndComp
Text Label 8400 1650 2    50   ~ 0
DIN
$Comp
L power:GNDREF #PWR010
U 1 1 5BDE316E
P 8300 1950
F 0 "#PWR010" H 8300 1700 50  0001 C CNN
F 1 "GNDREF" H 8305 1777 50  0000 C CNN
F 2 "" H 8300 1950 50  0001 C CNN
F 3 "" H 8300 1950 50  0001 C CNN
	1    8300 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5BDF630D
P 8300 1350
F 0 "#PWR02" H 8300 1200 50  0001 C CNN
F 1 "+5V" H 8315 1523 50  0000 C CNN
F 2 "" H 8300 1350 50  0001 C CNN
F 3 "" H 8300 1350 50  0001 C CNN
	1    8300 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 1550 8300 1550
Wire Wire Line
	8300 1550 8300 1350
Wire Wire Line
	8400 1750 8300 1750
Wire Wire Line
	8300 1750 8300 1950
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 5BE28AD2
P 8900 1650
F 0 "J4" H 8900 1850 50  0000 C CNN
F 1 "CHAIN" H 8980 1601 50  0001 L CNN
F 2 "" H 8900 1650 50  0001 C CNN
F 3 "~" H 8900 1650 50  0001 C CNN
	1    8900 1650
	-1   0    0    -1  
$EndComp
Text Label 9100 1650 0    50   ~ 0
DOUT
$Comp
L power:GNDREF #PWR011
U 1 1 5BE28AD9
P 9200 1950
F 0 "#PWR011" H 9200 1700 50  0001 C CNN
F 1 "GNDREF" H 9205 1777 50  0000 C CNN
F 2 "" H 9200 1950 50  0001 C CNN
F 3 "" H 9200 1950 50  0001 C CNN
	1    9200 1950
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5BE28ADF
P 9200 1350
F 0 "#PWR03" H 9200 1200 50  0001 C CNN
F 1 "+5V" H 9215 1523 50  0000 C CNN
F 2 "" H 9200 1350 50  0001 C CNN
F 3 "" H 9200 1350 50  0001 C CNN
	1    9200 1350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9100 1550 9200 1550
Wire Wire Line
	9200 1550 9200 1350
Wire Wire Line
	9100 1750 9200 1750
Wire Wire Line
	9200 1750 9200 1950
$EndSCHEMATC
